<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Haralds Blog &mdash; Geometrized Units</title>
    <link rel="icon" href="/blog/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/blog/images/RedOrangPercent-32x32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/blog/images/RedOrangPercent-64x64.png">
    <link rel="alternate" type="application/rss+xml" title="miamao's RSS feed" href="/blog/rss.xml">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link type="text/css" rel="stylesheet" href="/blog/style.css">
    <link rel="stylesheet" href="/blog/js-gen/github.min.css">
    <script type="text/javascript" src="/blog/js-gen/highlight.min.js"></script>
    <script>
      MathJax = {
        tex: {
          inlineMath: [['$','$']],
          displayMath: [['$$', '$$']],
          tags: "ams"
        },
        chtml: {
          // scale: 1.4,
          // minScale: 1.4
        }
      };
    </script>
    
    <script
      src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
      type="text/javascript">
    </script>
    <!-- FIXME: replaceAbsoluteHrefs should apply to src= links too -->
    <script type="module" src="/blog/js-gen/main.js">
    </script>
    </head>
  <body>
    <div style="display:none;">$
      \def\Vec#1{\mathbf{#1}}
      \def\vt#1{\Vec{v}_{#1}(t)}
      \def\v#1{\Vec{v}_{#1}}
      \def\vx#1{\Vec{x}_{#1}}
      \def\av{\bar{\Vec{v}}}
      \def\vdel{\Vec{\Delta}}
      $
    </div>

    <div class="header">
      <h1><a href="/blog/">Harald Kirsch</a></h1>
      <a class="about" href="/blog/About.html">about this blog</a>
    </div>
    <div class="middle">
      <div class="sitetoc hidden">
        <a target="_blank" href="https://harald.codeberg.page/arrangimage/">Image Stitch App</a>
        <h1 class="offButton"><span>Pages</span>
          <button class="sitetoc-button">
            <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
          </button>
        </h1>
        <a class="livetemplate" href="/blog/sitetoc.html">site table of contents</a>
      </div>
      <div class="article">
        <button class="sitetoc-button onButton">
          <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
        </button>
        <!-- don't insert space in the button ! -->
        <button class="tootbutton"><a class="livetemplate" href="/blog/images/mastodon-logo-purple.svg">svg</a></button>
        <div class="date">2024-04-18</div>
<h1 class="title">Geometrized Units</h1>

<p>When reading about theoretical physics, like relativity or quantum
mechanics, sooner or later you come across statements like "... with
$c=1$ ..." meaning the speed of light is set to be $1$. And
  similarly, for example with the gravitational constant: $G=1$.</p>

<p>This always made me grind my teeth, because: "One? One what?" And also
I take some confidence from checking units in a formula for better
understanding. If $c$ and $G$ just disappear, this can be quite
confusing. Or,
as <a href="https://www.seas.upenn.edu/~amyers/NaturalUnits.pdf">Alan
    L Myers</a> puts it:</p>

<blockquote>However, for a person educated in the SI system of units,
the lingo of “natural units” used by cosmologists is confusing and
seems (incorrectly) to display a casual disregard of the importance of
units in calculations.</blockquote>

<p>This PDF by Myers is quite formal, which is nice, but still it left me
scratching my head a bit. So I toyed around with the presented ideas
to come up with an explanation I personally like. Whether it is 100%
  correct, I can't say, but ... well.</p>

<h2>What follows directly from setting $c=1$ ?"</h2>
$$\def\um{\text{m}}
\def\us{\text{s}}
\def\ukg{\text{kg}}
\def\uN{\text{N}}$$


<p>Myers writes two things in different formulas, which, together amount
  to:</p>

$$1 = c = 2.9979\cdot 10^8\, \text{m/s}\strut$$

<p>And to not carry around the unwieldy number, lets call it $c_0 :=
2.9979\cdot 10^8$, so $c_0$ is really just shortcut for a
number. Similarly, lets have $G_0$ as the numerical value of the
  gravitional constant such that we get:</p>

\begin{align*}
1 = c &= c_0 \frac{\um}{\us}\\
1 = G &= G_0 \frac{\um^3}{\ukg\,\us^2}
\end{align*}

<p>Taking this formally seriously, we see that</p>

$$1\,\us = c_0\,\um\,$$

<p>which basically tells us that <em>meter</em> and <em>second</em> are
now "the same" unit connected by a mere dimensionless conversion
factor. Distance and time are the same thing and can be converted into
each other with the speed of light as the numerical factor. Which, for
everyone having heard the term <em>space-time</em>, should not be a
  surprise. Fine.</p>

<p>What about the $G$? Rearranging the formula we get</p>

\begin{align*}
1 &= G_0 \frac{\um^3}{\ukg\,\us^2} \\
\Leftrightarrow\quad 1\,\ukg &= G_0 \frac{\um^3}{\us^2} \\
\Leftrightarrow\quad 1\,\ukg &= \frac{G_0}{c_0^2}\,\um
\end{align*}

<p>telling us that the units of mass (or energy) and length are the same
  too. Weird.</p>

<p>Lets try some more.</p>

<h3>Force in Newton</h3>
Force is measured in Newton, defined as $1\,\uN =
\frac{\ukg\,\um}{\us^2}$, so we get

\begin{align*}
1\,\uN &= \frac{G_0}{c_0^2}\frac{\um^2}{\us^2}\\
       &= \frac{G_0}{c_0^4}
\end{align*}

<p>which makes force dimensionless.</p>

<h3>Schwarzschild Radius</h3>
<p>The Scharzschild Radius conventionally is $r_s = 2GM/c^2$ which
reduces to $r_s = 2M$ and luckily, mass is measured in $\um$ now, so
  this fits.</p>

<h3>Planck's Constant</h3>
<p>If, similar to the above, we chose the number $h_0$ such that in SI
units we have $h=h_0\, \ukg\,\um^2/\us$, we get, by replacing the mass
  and one velocity term:</p>

\begin{align*}
h &= h_0 \frac{G_0}{c_0^2}\,\um \frac{1}{c_0}\um \\
  &= \frac{h_0 G_0}{c_0^3}\um^2\,.
\end{align*}


<h2>Personal Comment</h2>

<p>Time has told that the above system of units makes sense and helps to
make equations simpler. I think I understand it now a bit better. The
PDF cited above formalises how to replace combinations of $\ukg^\alpha
\um^\beta \us^\gamma$ easily with combinations of $G$ and
$c$ to see the resulting unit, but it helped me to just do it step by
  step for a few examples.</p>


<p>Yet:</p>

<ul>
  <li>It feels like cheating, but that's probably because I am not
    used to this.</li>
  <li>I am somewhat surprised that this works. While diluting the
  difference between time and length seems well founded by what we
  know from General Relativity, having mass and length "the same" is
    weird.</li>
  <li>If we would, somewhat arbitrarily, equate $\ukg$ and $1/\us$,
  would we still have working physics math, if for the price of
  over-complex equations? Or would this lead to contradictions,
  meaning the above selection is not as arbitrary as it looks &mdash;
    apart from it making formulars slicker?
  </li>
</ul>

<p>And finally, the above gymnastics raise the question: what are
physical units anyway, in particular if we can dissolve them into each
  other so easily?</p>


      </div>
    </div>
    <div class="footer">
      <span>&copy;2022,2023,2024 Harald Kirsch</span>
      <span>Blog's <a href="https://codeberg.org/harald/pages" target="_blank">source code</a></span>
    </div>
  </body>
</html>
