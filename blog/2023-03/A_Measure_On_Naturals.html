<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Haralds Blog &mdash; A Measure on the Naturl Numbers</title>
    <link rel="icon" href="/blog/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/blog/images/RedOrangPercent-32x32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/blog/images/RedOrangPercent-64x64.png">
    <link rel="alternate" type="application/rss+xml" title="miamao's RSS feed" href="/blog/rss.xml">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link type="text/css" rel="stylesheet" href="/blog/style.css">
    <link rel="stylesheet" href="/blog/js-gen/github.min.css">
    <script type="text/javascript" src="/blog/js-gen/highlight.min.js"></script>
    <script>
      MathJax = {
        tex: {
          inlineMath: [['$','$']],
          displayMath: [['$$', '$$']],
          tags: "ams"
        },
        chtml: {
          // scale: 1.4,
          // minScale: 1.4
        }
      };
    </script>
    
    <script
      src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
      type="text/javascript">
    </script>
    <!-- FIXME: replaceAbsoluteHrefs should apply to src= links too -->
    <script type="module" src="/blog/js-gen/main.js">
    </script>
    </head>
  <body>
    <div style="display:none;">$
      \def\Vec#1{\mathbf{#1}}
      \def\vt#1{\Vec{v}_{#1}(t)}
      \def\v#1{\Vec{v}_{#1}}
      \def\vx#1{\Vec{x}_{#1}}
      \def\av{\bar{\Vec{v}}}
      \def\vdel{\Vec{\Delta}}
      $
    </div>

    <div class="header">
      <h1><a href="/blog/">Harald Kirsch</a></h1>
      <a class="about" href="/blog/About.html">about this blog</a>
    </div>
    <div class="middle">
      <div class="sitetoc hidden">
        <a target="_blank" href="https://harald.codeberg.page/arrangimage/">Image Stitch App</a>
        <h1 class="offButton"><span>Pages</span>
          <button class="sitetoc-button">
            <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
          </button>
        </h1>
        <a class="livetemplate" href="/blog/sitetoc.html">site table of contents</a>
      </div>
      <div class="article">
        <button class="sitetoc-button onButton">
          <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
        </button>
        <!-- don't insert space in the button ! -->
        <button class="tootbutton"><a class="livetemplate" href="/blog/images/mastodon-logo-purple.svg">svg</a></button>
        <div class="date">2023-03-27</div>
<h1 class="title">A Measure on the Naturl Numbers</h1>

<h2 id="into">Where am I coming from</h2>
<div style="display: none">$
  \def\NN{\mathbb{N}}
  \def\N0{\mathbb{N}_0}
  \def\RR{\mathbb{R}}
  \def\DD{\cal{D}}
  \def\Mset(#1,#2){(#1\kern 0.1em\NN-#2)}
  \def\lcm{\mathop{\mathrm{lcm}}\nolimits}
  $
</div>
<p>The other day I wondered why I don't know of a measure on the
natural numbers $\NN$. On the reals, $\RR$, we have a measure
deriving from the length of intervals as $\mu([a,b]) = b-a$. On $\NN$
we can assign a size to a subset by the number of elements it contains
&mdash; for finite subsets. Is there something possible for infinite
  subsets.</p>

<h2 id="definition">Definition of a System of Sets</h2>
<p>For $k\in\NN$ let $k\NN := \{kn \,|\, n\in\NN\}$, so $7\NN = \{7, 14,
21, \dots\}$. We also allow to shift such sets to the left to be able
  to start at $1$:</p>

$$\Mset(k,i) :=  \{kn -i \,|\, n\in\NN\}
\quad\text{ for } k\in\NN \text{ and } 0\leq i\lt k.$$

<p>So $\Mset(5,4) = \{1, 6, 11, \dots\}$.
  For convenience lets define</p>

$$\DD = \{\Mset(k,i) \,|\, k\in\NN \land 0\leq i\lt k\}.$$

<p>With union over the shifts $i$ we get the natural numbers:</p>

$$\NN = \bigcup_{i=0}^{k-1} \Mset(k,i).$$

<p>But what happens with the intersection of two set of $\DD$?</p>

<p><b>Proposition:</b> For two set $A, B\in\DD$ we have
  either $ A\cap B = \emptyset$ or $A\cap B \in \DD$.</p>

<p><b>Proof:</b> Suppose $A = \Mset(k,i)$ and $B=\Mset(l,j)$ are not
disjoint. Then let $s$ be the smallest element in $ A\cap B$. By
  definition of $A$ and $B$ we can find $n_k, n_l\in\NN$ such that</p>

$$
s = kn_k -i = ln_l -j \qquad \text{with } i,j\in\NN.
$$

<p>Let $q = \lcm(k, l)$ be the least common multiple of $k$ and $l$. Then
  we can find  $0\leq r \lt q$ and $t\in\NN$ such that $s = qt - r$. Namely chose $t = \lfloor\frac{s}{q}\rfloor + 1$ and $r = s - tq$.</p>

<p>Assume that $t>1$. Then define $s_0 = q - r \lt s$ and we have</p>

\begin{align*}
s - s_0 &= qt - r -(q -r)\\
&= q(t-1)\\
\text{or}\qquad s_0 &= s - q(t-1)
\end{align*}                                              

<p>Since $q=\lcm(k,l)$ we can write $q=q_kk=q_ll$ for some $q_k,
  q_l\in\NN$ and so we get</p>

\begin{align*}
s_0 &= s - q_kk(t-1) &&= s - q_ll(t-1)\\
&=  kn_k -i - q_kk(t-1) &&= ln_l -j - q_ll(t-1)\\
&= k(n_k -q_kk(t-1)) -i &&= l(n_l -q_ll(t-1)) -j\\
\end{align*}

<p>This shows that $s_0\in \Mset(k,i)\cap\Mset(l,j)$ though $s$ was defined
  to be the smallest of those. Therefore we have actually $s=s_0= q-r$.</p>

<p>We now show that $A\cap B = \Mset(q,r)$.</p>

<p><i>Backward direction:</i> Let $t'\in\NN$ and define $s'=qt' -
r\in\Mset(q,r)$. Then we have $$s'-s = qt'-r - (q-r) = q(t' -1).$$
  Similar to the reasoning above it follows that</p>

\begin{align*}
s' &= s + k q_k(t'-1)      &&= s + l q_l(t'-1) \\
&= kn_k - i + k q_k(t'-1) &&= ln_l -j + l q_l(t'-1)\\
&= k(n_k+q_k(t'-1))-i &&= l(n_l+q_l(t'-1)) -j\\
\end{align*}

<p>And since $t'-1\ge 0$ this shows that $s'\in A\cap B$.</p>

<p><i>Forward direction:</i> Let $s'\in \Mset(k,i)\cap\Mset(l,j)$ so we
  have</p>

$$ s' = kn_k' - i = ln_l' -j. $$

<p>It follows that</p>

\begin{align*}
s'-s &= kn_k' - i - (kn_k-i)&&= ln_l' - j -(ln_l-j) \\
     &= k(n_k'-n_k) && = l(n_l'-n_l)
\end{align*}

<p>Consquently $s' -s$ is a multiple of both, $k$ and $l$, which means
$q=\lcm(k,l)$ is a factor of $s' - s$, say $s'-s = q_sq$. But then we
  have</p>

\begin{align*}
s' &= s + q_sq\\
&= q - r + q_sq\\
&= q(q_s + 1) -r\\
& \in\Mset(q,r).
\end{align*}
<div style="text-align: right"><b>q.e.d</b></div>


      </div>
    </div>
    <div class="footer">
      <span>&copy;2022,2023,2024 Harald Kirsch</span>
      <span>Blog's <a href="https://codeberg.org/harald/pages" target="_blank">source code</a></span>
    </div>
  </body>
</html>
