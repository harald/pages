<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Haralds Blog &mdash; Developing Simple Web Applications</title>
    <link rel="icon" href="/blog/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/blog/images/RedOrangPercent-32x32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/blog/images/RedOrangPercent-64x64.png">
    <link rel="alternate" type="application/rss+xml" title="miamao's RSS feed" href="/blog/rss.xml">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link type="text/css" rel="stylesheet" href="/blog/style.css">
    <link rel="stylesheet" href="/blog/js-gen/github.min.css">
    <script type="text/javascript" src="/blog/js-gen/highlight.min.js"></script>
    <script>
      MathJax = {
        tex: {
          inlineMath: [['$','$']],
          displayMath: [['$$', '$$']],
          tags: "ams"
        },
        chtml: {
          // scale: 1.4,
          // minScale: 1.4
        }
      };
    </script>
    
    <script
      src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
      type="text/javascript">
    </script>
    <!-- FIXME: replaceAbsoluteHrefs should apply to src= links too -->
    <script type="module" src="/blog/js-gen/main.js">
    </script>
    </head>
  <body>
    <div style="display:none;">$
      \def\Vec#1{\mathbf{#1}}
      \def\vt#1{\Vec{v}_{#1}(t)}
      \def\v#1{\Vec{v}_{#1}}
      \def\vx#1{\Vec{x}_{#1}}
      \def\av{\bar{\Vec{v}}}
      \def\vdel{\Vec{\Delta}}
      $
    </div>

    <div class="header">
      <h1><a href="/blog/">Harald Kirsch</a></h1>
      <a class="about" href="/blog/About.html">about this blog</a>
    </div>
    <div class="middle">
      <div class="sitetoc hidden">
        <a target="_blank" href="https://harald.codeberg.page/arrangimage/">Image Stitch App</a>
        <h1 class="offButton"><span>Pages</span>
          <button class="sitetoc-button">
            <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
          </button>
        </h1>
        <a class="livetemplate" href="/blog/sitetoc.html">site table of contents</a>
      </div>
      <div class="article">
        <button class="sitetoc-button onButton">
          <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
        </button>
        <!-- don't insert space in the button ! -->
        <button class="tootbutton"><a class="livetemplate" href="/blog/images/mastodon-logo-purple.svg">svg</a></button>
        <div class="date">2023-12-18</div>
<h1 class="title">Developing Simple Web Applications</h1>

<p>Recently I got interested in what it takes to implement simple web
applications without the waggon-loads of framework software typically used
these days. The following describes an approach which works
  surprisingly well.</p>

<h2>How?</h2>

<h3>Use Typescript</h3>

<p>Coming from a Java background, I feel much better when the compiler
and, even more, the IDE can help to avoid stupid mistakes and
moreoever tell me a lot about intent and functionality of variables
  and functions by showing their types.</p>

<p>Alas for what follows, using Typescript is not crucial. Plain
  Javascript can be used in the same way.</p>

<h3>Implement Components</h3>

The following list summarizes the concept. Discussion and rationale is
provided afterwards.

<ol>
  <li>Create components as Java-/Typescript classes.</li>
  <li>Let such a component manage a piece of DOM, typically with its root
    element and a few descendants.</li>
  <li>The component may be a container where some of the descendants
    are provided by other components.</li>
  <li>Typically a component has a <code>getElement():
  HTML...Element</code> method to provide the root element of the DOM
  part it manages.
  <li>A component should have a clean API for other code to manipulate
  it. Direct event bindings should be scarcely used. For example a
  component providing visual list manipulation: to select a list
  element, the component could bind the list elements to
  an <code>onclick</code> event, but by providing
  a <code>selectElement()</code> method it allows other ways of
  selection than by clicking.</li>
  <li>Make use of components which have no DOM elements to manipulate
  of their own, but are rather controllers (in the
  model-view-controller abstraction) which take care of event handling
  and then call APIs of DOM-care-takers in response to UI events.</li>
  <li>Use <code>document.createElement()</code> to create the DOM
  elements a component wants to directly manipulate. Do not use
  template engines.</li>
  <li>Let a component add the minimum of CSS rules that are needed to
  let the managed piece of DOM <b>work</b> as intented. It does
  not need to look nice from the component-added CSS alone.</li>
  <li>These component-added CSS rules shall be inserted at
  the <b>front</b> of the the <code>&lt;head></code> element. This
  allows style sheet links with rules that override the rules added by
  the components.</li>
  <li>The component Javascript class should add a sufficiently
    "unique" CSS class to the root of the DOM piece it manages. This
    could be, for example, the name of the Javascript class.</li>
  <li>Whether the components are implemented
  as <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Components">Web
  Components</a> or rather favor composition over inheritance is a
  matter of taste. But good taste suggests to avoid Web Components as
  they are rather complicated while not adding a lot, if any,
  benefit.</li>
  <li>Don't let a component tweak HTML elements it does not own. If
  there are things to adapt, to change, to tweak, to configure, the
  owner component should have a respective API.</li>
</ol>

<h2>Where is the framework?</h2>

<p>Right <a href="https://developer.mozilla.org/en-US/">there</a>. The
whole idea is to get away without using tons of framework code. Just
  follow the simple guidelines outlined above.</p>

<p>It would obviously be nice if there were already components written
in the style described above with well defined behaviour, independent
of any framework, very scarce dependencies on other components, all
interoperable because all they provide are a Javascript class each
  with a simple API to operate them.</p>

<h2>Why?</h2>
<p>At work we developed (web-)app software with Typescript, Angular,
Cordova, Ionic and whatnot. It shall make things easier and
  development quicker and more reliable.</p>

<p>It works for the strong typing which Typescript brings in.</p>

<p>Angular and Ionic, on the other hand, were often the enemy we had to
fight to get what we and/or the customer wanted. Both shall reduce
  complexity, but the price is (a) more complexity plus (b) magic.</p>

<p>If the magic just works, great. But too many times the magic didn't
work as needed and we had to unravel the inner workings of magic to
  figure out how to nudge it to do what was required.</p>

<p>So I started to wonder whether the cost matched the value. Obviously
this depends on so many factors that the outcome is different for
  everyone. Plus: no sane head of development would say these days:</p>

<blockquote>"no frameworks, we use plain Typescript and the odd small
  library as long as it does not dictate our code structure, provides
  no magic and has a sane API to call."</blockquote>

<p>Because there are always problems. With "industry standard"
frameworks, the head of development can report: "this is normal, its
complex software, there is a reported bug and BigCompany wants to fix
  it in release 1.2.3."</p>

<p>With custom made components, there is nobody to blame outside the
company if shit happens.  But for my private projects, I can do what I
  want, so I started to follow the guidelines above.</p>

<p>Works for me.</p>

<h2>More Detailed "How?"</h2>
<p>Historically a web page was just that, a page of HTML. Then came some
active content, like Javascript, then came frameworks which allowed to
pepper HTML templates heavily with code. Lets now finish this shift:
  don't write HTML at all, write pure Java-/Typescript.</p>

<p>These components need not
be <a href="https://developer.mozilla.org/en-US/docs/Web/API/Web_Components">Web
Components</a> which extend from <code>HTMLElement</code> or its
descendants. It works well if a visual component is represented by a
Javascript class which creates a small part of the DOM tree and
provides the root of the tree for other components to retrieve and add
to their own part of the tree. As an example lets assume we have
a <code>ModalMenu</code> component and a component providing the
content to be shown, lets say <code>FileMenu</code>. The code to use
  these might look like:</p>

<pre class="lang-typescript">
const modal = new ModalMenu(...);
const fileMenu = new FileMenu(...);
modal.show(fileMenu.getElement());
</pre>

<p>The idea is that <code>ModalMenu.show()</code> accepts
an <code>HTMLElement</code> to inject into some wrapper, maybe
  a <code>HTMLDialogElement</code> and then shows in on screen.</p>

<p>To create the HTML elements, the component classes may use whatever
works best. But consider to just
use <code>document.createElement()</code> calls, which has at least
the following benefits over templating (see more about
  this <a href="#avoid-templates">below</a>):</p>

<dl>
  <dt>Less typing hassles in Typescript</dt>
  <dd>By using
    <pre class="lang-typescript">const button = documentCreateElement('button');</pre>
    the <code>button</code> immediately has the right
    type, <code>HTMLButtonElement</code>. When using a template
    HTML structure a <code>document.querySelector('#gooButton')</code> returns
     <code>Element|null</code> which is quite annoying in
    Typescript. But even when using Javascript, the problem may
    arise that the element ID in the template is changed
    to <code>#fooButton</code> which requires the insight to know
    there is a query selector used to retrieve it and that code must
    be changed too. And eventually, it won't.
  </dd>
  <dt>Fewer DOM elements</dt>
  <dd>We're all lazy. If each element of the
  DOM needs a separate <code>createElement</code> we somewhat try to
    avoid mushrooming of <code>&lt;div&gt;</code> wrappers :-)</dd>
  <dt>Cleaner code</dt>
  <dd>For the same lazyness reason we're pushed to
  extract repeated element creation into short, understandable
  functions while a template rather suggests to use
    copy&amp;paste&amp;screwup coding for similar repeated elements.
  </dd>
</dl>

<h3>CSS how?</h3>

<p>Let the components add the CSS which is absolutely necessary for their
function to front of the <code>&lt;head></code> element. Think of a
  minimum size for an element to be tappable with fat fingers.</p>

<p>Non-functional styling for the good look should go into a normal style
sheet. As long as it is loaded after the CSS injected into the head,
  the style sheet can still override the injected rules.</p>

<p>Each component should add a "unique" CSS class name to the root
element(s) it manages. Here everyone cries "foul" and that conflicts
are to be expected. Interestingly, though, fully qualified class names
in the Java world manage for decades to stay different from each
other with maybe very few exceptions. So if you start to develop a
library along the advice suggested here, you may start to mimic the
scheme and use class names
  like <code>de_mydomain_MenuButton</code>.</p>

<h3>Two types of components</h3>
<p>Similar to not having to much styling to generate a specific look, a
component should also be scarce in which user interaction it provides
via event bindings. If the user shall interact with some elements
managed by the component, first add an API to the component under the
assumption that other components want to initiate the
interaction. Whether the component provides default event bindings or
  not can be decided afterwards.</p>

<p>As a result you will note that you naturally get two kinds of
components: those with DOM elements and those which deal with event
handling. It may well be OK to not separate these for small
components. A button to initiate a foobleblarg is just that: a button
  with an action to take. No need to separate the action out.</p>

<p>But consider component <code>MarliworgList</code> maintaining and
displaying a list of marliworgs. Deleting an element from the list
should be first and formost an API of the
class <code>MarliworgList</code>. As should be the selection of list
elements. Which user interaction initiates the selection and deletion
of elements is a completely different story. Whether some button,
whether a mouse click, whether an incremental search box: there may be
  many ways to select and delete elements and all of these</p>

<ul>
  <li>need not be part of <code>MarliworgList</code> and</li>
  <li>should solely use the API provided
    by <code>MarliworgList</code>.
  </li>
</ul>


<h3 id="avoid-templates">Avoid Templates</h3>

<p>Do not use templates.</p>

<ul>
  <li>Designers no longer provide "working" HTML. The (very) old
  workflow of getting some HTML from designers and tweak it into
  templates does not exist anymore (I hope). The HTML is written by
  the developer anyway, so read on why it should not be done with
  templates.</li>
  <li>Template engines all include crude and often disgusting code
    constructs for conditionals and loops.</li>
  <li>Those constructs are nothing but a badly designed programming
  language.</li>
  <li>Template text becomes a horrible mix of static code and these
    imperative programming constructs.</li>
  <li>JSP was a mix of XML, HTML, Java and the expression language
    (did I miss one?)</li>
  <li>Angular throws in <code>([thing])=thang</code>, <code>*if</code>
    and <code>*for</code> and <code>async</code> and pipes to create
    template code which resembles HTML only when squinting really
    hard.</li>
</ul>

<p>Just use <code>document.createElement()</code> calls wrapped in
suitably designed (code wise) Type-/Javascript classes. Keep the DOM
elements you need to manipulate in class fields, stick the others
right into the DOM snippet the Javascript class is managing and forget
about them. Make use of the full power of Type-/Javascript for clean
  code when creating DOM elements instead of weird template syntax.</p>

<h2>Show me the code</h2>
<p>See <a target="_blank" href="https://codeberg.org/harald/arrangimage/src/branch/main/src/app">arrangimage</a>
as an application where I used the approach described above. <b>Ignore
  that I use a class <code>Template</code></b>, it is a relic of an
  initial mistake which needs to be removed.</p>

      </div>
    </div>
    <div class="footer">
      <span>&copy;2022,2023,2024 Harald Kirsch</span>
      <span>Blog's <a href="https://codeberg.org/harald/pages" target="_blank">source code</a></span>
    </div>
  </body>
</html>
