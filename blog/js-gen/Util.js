export function addStyleElementToHead(style) {
    if (style === undefined) {
        return;
    }
    const styleEl = document.createElement('style');
    styleEl.textContent = style;
    document.head.prepend(styleEl);
}
//# sourceMappingURL=Util.js.map