var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { Toot } from './Toot.js';
import { createTranslator } from './Translations.js';
const τ = createTranslator('de');
function startup() {
    return __awaiter(this, void 0, void 0, function* () {
        const dirToggles = document.querySelectorAll('.directory .l1');
        for (const el of dirToggles) {
            if (!(el instanceof HTMLElement)) {
                continue;
            }
            const toToggle = el.parentElement;
            if (toToggle === undefined || toToggle === null) {
                continue;
            }
            el.onclick = () => toToggle.classList.toggle('folded');
        }
        yield injectLiveTemplates();
        withAll('.preview', (el) => (el.onclick = () => fullPreviewClick(el)));
        withEl('.sitetoc', (sitetoc) => {
            withAll('.sitetoc h1, .article .sitetoc-button', (el) => (el.onclick = () => toggleSiteToc(sitetoc)));
        });
        if (null !== document.querySelector('.preview')) {
            withEl('.sitetoc-button', (el) => el.click());
        }
        withAll('mjx-container', (el) => {
            const sw = el.scrollWidth;
            const w = 2 + el.getBoundingClientRect().width;
            el.classList.toggle('scrollable', sw > w);
        });
        document.querySelectorAll('pre[class^=lang-]').forEach((el) => {
            hljs.highlightElement(el);
        });
        addMastodonTootButton();
    });
}
function toggleSiteToc(sitetoc) {
    sitetoc.classList.toggle('hidden');
}
function withEl(selector, f) {
    const el = document.querySelector(selector);
    if (el instanceof HTMLElement) {
        f(el);
    }
}
function withAll(selector, f) {
    elWithAll(document, selector, f);
}
function elWithAll(root, selector, f) {
    root.querySelectorAll(selector).forEach((el) => {
        if (el instanceof HTMLElement) {
            f(el);
        }
    });
}
function injectLiveTemplates() {
    const templates = document.querySelectorAll('[class="livetemplate"]');
    let head = Promise.resolve();
    for (const el of templates) {
        const href = el.getAttribute('href');
        if (href === null) {
            continue;
        }
        head = head
            .then(() => injectLiveTemplate(el, href))
            .catch((_error) => el.append('<span>failed to load + href</span>'));
    }
    return head;
}
function injectLiveTemplate(liveTemplate, href) {
    return fetch(href)
        .then((response) => {
        if (!response.ok) {
            throw new Error('did not get code 200');
        }
        return response.text();
    })
        .then((text) => {
        const part = document.createElement('template');
        part.innerHTML = text.trim();
        liveTemplate.replaceWith(part.content);
    });
}
function fullPreviewClick(el) {
    const ahref = el.querySelector('a[href]');
    if (ahref instanceof HTMLElement) {
        ahref.click();
    }
}
function addMastodonTootButton() {
    const toot = new Toot(τ, (value) => doneToot(value));
    document.body.append(toot.getElement());
    withEl('button.tootbutton', (el) => {
        el.append(τ('toot'));
        el.onclick = (evt) => doToot(toot, evt);
    });
}
function doToot(toot, evt) {
    evt.preventDefault();
    toot.show();
}
function doneToot(domain) {
    if (domain === undefined) {
        return;
    }
    if (domain === '' || domain === null) {
        return;
    }
    const url = new URL('https://' + domain + '/publish/');
    url.searchParams.append('text', '@haraldki@nrw.socal\n...' + τ('toot_filler') + '...\n\n' + location.href);
    window.open(url, '_blank');
}
document.addEventListener('DOMContentLoaded', () => startup(), {
    once: true,
});
//# sourceMappingURL=main.js.map