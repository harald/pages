import { addStyleElementToHead } from './Util.js';
export class AutoComplete {
    constructor(target, provider, onEnter) {
        this.target = target;
        this.provider = provider;
        this.onEnter = onEnter;
        this.acItems = [];
        addStyleElementToHead(AutoComplete.style);
        delete AutoComplete.style;
        this.attach(target);
        const acOnclick = (evt, term) => this.click(evt, term);
        const element = document.createElement('div');
        element.classList.add(AutoComplete.name);
        element.ontouchmove = (evt) => {
            evt.stopPropagation();
        };
        target.insertAdjacentElement('afterend', element);
        for (let i = 0; i < 60; i++) {
            const acItem = new AcItem(element, acOnclick);
            this.acItems.push(acItem);
            element.append(acItem.getElement());
        }
        this.element = element;
        this.hide();
    }
    hide() {
        this.element.classList.add('hidden');
    }
    show() {
        this.element.classList.remove('hidden');
    }
    attach(target) {
        target.oninput = (_evt) => this.render();
        target.addEventListener('keydown', (evt) => this.keypress(evt));
    }
    keypress(evt) {
        switch (evt.key) {
            case 'Enter': {
                this.keypressEnter(evt);
                return;
            }
            case 'ArrowDown': {
                this.keypressArrowDown(evt);
                return;
            }
            case 'ArrowUp': {
                this.keypressArrowUp(evt);
                return;
            }
            default:
                return;
        }
    }
    keypressArrowUp(evt) {
        this.doArrowUpDown(evt, (s) => s.previousElementSibling, undefined);
    }
    keypressArrowDown(evt) {
        var _a;
        const classList = (_a = this.element.firstElementChild) === null || _a === void 0 ? void 0 : _a.classList;
        this.doArrowUpDown(evt, (s) => s.nextElementSibling, classList);
    }
    doArrowUpDown(evt, fetchOther, classList) {
        evt.preventDefault();
        const selected = this.element.querySelector('.' + AutoComplete.cssSelected);
        if (selected === null) {
            classList === null || classList === void 0 ? void 0 : classList.add(AutoComplete.cssSelected);
            return;
        }
        const other = fetchOther(selected);
        if (other === null || other.classList.contains('hidden')) {
            return;
        }
        selected.classList.remove(AutoComplete.cssSelected);
        other.classList.add(AutoComplete.cssSelected);
        other.scrollIntoView({ block: 'center' });
    }
    keypressEnter(evt) {
        const selected = this.element.querySelector('.' + AutoComplete.cssSelected);
        if (selected === null) {
            return;
        }
        this.hide();
        if (selected instanceof HTMLElement) {
            this.valueWasSelected(selected.innerText);
        }
        selected.classList.remove(AutoComplete.cssSelected);
        evt.preventDefault();
    }
    click(evt, term) {
        this.valueWasSelected(term);
        this.hide();
        evt.preventDefault();
        evt.stopPropagation();
        evt.stopImmediatePropagation();
    }
    valueWasSelected(term) {
        this.target.value = term;
        this.onEnter();
    }
    render() {
        const terms = this.provider(this.target.value);
        if (terms.length === 0) {
            this.hide();
            return;
        }
        this.acItems.forEach((item) => item.hide());
        let i = 0;
        for (const term of terms) {
            const acItem = this.acItems[i++];
            if (acItem === undefined) {
                break;
            }
            acItem.setTerm(term);
        }
        const horizMargin = 6;
        const xpos = horizMargin + this.target.offsetLeft;
        const ypos = this.target.offsetTop + this.target.offsetHeight;
        this.element.style.left = xpos + 'px';
        this.element.style.top = ypos + 'px';
        this.element.style.width = this.target.clientWidth - 2 * horizMargin + 'px';
        this.show();
    }
}
AutoComplete.cssSelected = 'acSelected';
AutoComplete.style = `
  .AutoComplete {
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0;
    position: absolute;
    background: inherit;
    border: 1px solid #aaa;
    box-shadow: -4px 4px 3px grey;
    cursor: pointer;
    overflow-y: scroll;
    max-height: 150px;
  }
  .AutoComplete div {
    padding-left: 4px;
    line-height: 28px;
  }
  .AutoComplete div:nth-child(odd) {
    background-color: color-mix(in srgb, #000 4%, var(--background));
  }
  .AutoComplete div.${AutoComplete.cssSelected} {
    background: var(--selected-element, #aba);
  }
  `;
class AcItem {
    constructor(parent, onClick) {
        this.element = document.createElement('div');
        parent.append(this.element);
        this.element.onclick = (evt) => onClick(evt, this.getTerm());
    }
    getTerm() {
        return this.element.innerText;
    }
    setTerm(term) {
        this.element.innerText = term;
        this.show();
    }
    getElement() {
        return this.element;
    }
    hide() {
        this.element.classList.add('hidden');
        this.element.classList.remove(AutoComplete.cssSelected);
    }
    show() {
        this.element.classList.remove('hidden');
    }
}
//# sourceMappingURL=AutoComplete.js.map