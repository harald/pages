const translationTable = {
    toot: {
        en: 'toot',
        de: 'Tröt',
    },
    mastodonDomainPrompt: {
        en: 'Toot on which server',
        de: 'Auf welchem Server tröten?'
    },
    toot_filler: {
        en: 'tell me what you think',
        de: 'schreib mir was',
        fr: 'écris moi'
    },
    Cancel: {
        en: 'Cancel',
        de: 'Abbrechen'
    },
    OK: {
        en: 'OK',
        de: 'OK'
    },
    'e.g.': {
        en: 'e.g.',
        de: 'z.B.'
    }
};
const allLanguages = new Set(Object.keys(translationTable.toot));
for (const key in translationTable) {
    const others = Object.keys(translationTable[key]);
    for (const lang of allLanguages) {
        if (!others.includes(lang)) {
            allLanguages.add(lang);
        }
    }
}
function isLanguage(text) {
    return allLanguages.has(text);
}
function translate(language, fallbackLanguage, tag) {
    const entry = translationTable[tag];
    return entry[language] || entry[fallbackLanguage] || tag;
}
export function createTranslator(defaultLanguage) {
    let userLang = navigator.language;
    if (userLang.length > 2) {
        userLang = userLang.substring(0, 2);
    }
    if (isLanguage(userLang)) {
        const language = userLang;
        return (tag) => translate(language, defaultLanguage, tag);
    }
    return (tag) => translate(defaultLanguage, defaultLanguage, tag);
}
//# sourceMappingURL=Translations.js.map