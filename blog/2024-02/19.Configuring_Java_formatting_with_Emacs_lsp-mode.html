<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Haralds Blog &mdash; Configuring Java formatting with Emacs lsp-mode</title>
    <link rel="icon" href="/blog/images/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="/blog/images/RedOrangPercent-32x32.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/blog/images/RedOrangPercent-64x64.png">
    <link rel="alternate" type="application/rss+xml" title="miamao's RSS feed" href="/blog/rss.xml">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link type="text/css" rel="stylesheet" href="/blog/style.css">
    <link rel="stylesheet" href="/blog/js-gen/github.min.css">
    <script type="text/javascript" src="/blog/js-gen/highlight.min.js"></script>
    <script>
      MathJax = {
        tex: {
          inlineMath: [['$','$']],
          displayMath: [['$$', '$$']],
          tags: "ams"
        },
        chtml: {
          // scale: 1.4,
          // minScale: 1.4
        }
      };
    </script>
    
    <script
      src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"
      type="text/javascript">
    </script>
    <!-- FIXME: replaceAbsoluteHrefs should apply to src= links too -->
    <script type="module" src="/blog/js-gen/main.js">
    </script>
    </head>
  <body>
    <div style="display:none;">$
      \def\Vec#1{\mathbf{#1}}
      \def\vt#1{\Vec{v}_{#1}(t)}
      \def\v#1{\Vec{v}_{#1}}
      \def\vx#1{\Vec{x}_{#1}}
      \def\av{\bar{\Vec{v}}}
      \def\vdel{\Vec{\Delta}}
      $
    </div>

    <div class="header">
      <h1><a href="/blog/">Harald Kirsch</a></h1>
      <a class="about" href="/blog/About.html">about this blog</a>
    </div>
    <div class="middle">
      <div class="sitetoc hidden">
        <a target="_blank" href="https://harald.codeberg.page/arrangimage/">Image Stitch App</a>
        <h1 class="offButton"><span>Pages</span>
          <button class="sitetoc-button">
            <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
          </button>
        </h1>
        <a class="livetemplate" href="/blog/sitetoc.html">site table of contents</a>
      </div>
      <div class="article">
        <button class="sitetoc-button onButton">
          <a class="livetemplate" href="/blog/images/dashedArrowhead.svg">button svg</a>
        </button>
        <!-- don't insert space in the button ! -->
        <button class="tootbutton"><a class="livetemplate" href="/blog/images/mastodon-logo-purple.svg">svg</a></button>
        <div class="date">2024-02-19</div>
<h1 class="title">Configuring Java formatting with Emacs lsp-mode</h1>

<p>Continuing the series about
  of <a href="/blog/2024-02/16.Most_Used_Coding_Shortcuts.html">configuring
  Emacs as a Java development IDE</a>, lets talk about code
  formatting.</p>

<p>Emacs' lsp-mode uses
the <a href="https://github.com/eclipse-jdtls/eclipse.jdt.ls">Eclipse
  language server</a> to stay informed about the Java code being
edited. This also includes the server's service to format part or all
of the Java code according to some rules. It employs Eclipse's code
formatter which has lots of options. When using the Eclipse UI, these
  can be tweaked visually in an extensive settings screen.</p>

<p>To use these settings with the language server, one can export the
settings file and use it with lsp-java. The following describes the
  steps.</p>

<h3>Create the configuration file</h3>

<p>To create the configuration file it seems easiest to start a recent
version of Eclipse and navigate to <code>Window -> Preferences ->
Formatter</code>. If you need this very description, you'll likely see
only one profile, select <code>Edit</code>, adapt the rules as needed,
change the name to, say, <code>blablub-format</code> to unlock
the <code>Export</code> button and save the file to,
  say, <code>~/.emacs.d/eclipse-formatter.xml</code>.</p>

<h3>Activating the format configuration in lsp-java</h3>

<p>There are two variables of lsp-java which need to be customized:</p>

<ul>
  <li><code>lsp-java-format-settings-url</code>: I provided the full
    absolute path to the
    file <code>.../.emacs.d/eclipse-formatter.xml</code></li>
  <li><code>lsp-java-format-settings-profile</code> should be the
  name of the formatting profile. Check the top of the generated file
  to be sure, you should see the name you provided as the profile
    name: <code>blablub-format</code> was the example above.</li>
</ul>

<p>You know how to customize variables in Emacs, right? As a reminder,
you can use <key>M-x customize-variable</key> and type in the name
to get to the customization buffer. Alternatively use <key>M-x
  customize-group</key>, type in lsp-mode and find the variables
by their nice names which are easily derivable from the code names
  above.</p>

<h3>Restart the language server</h3>

<p>Switch to a Java buffer which is
managed by the language server and use <key>M-x
lsp-restart-workspace</key> or <key>s-l w r</key>. For a try,
use <key>s-l = =</key> to format the whole Java buffer. If nothing
happens, either your formatting is already along the rules or
  something went wrong. To quickly check you could toggle, for example,</p>

<pre class="lang-xml">
  &lt;setting
    id="org.eclipse.jdt.core.formatter.brace_position_for_block"
    value="end_of_line"
  />
</pre>

<p>between <code>end_of_line</code> and <code>next_line</code> by editing
the formatter file. Make sure to restart the language server after
  changes, as these do not seem to be picked up automatically.</p>

<p>To avoid the detour through Eclipse it may sometimes be enough to find
a configuration value in the <a target="_blank"
   href="https://help.eclipse.org/latest/index.jsp?topic=%2Forg.eclipse.jdt.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fjdt%2Fcore%2Fformatter%2FDefaultCodeFormatterConstants.html">Eclipse
  documentation</a>, but the best bet is probably to store the format
  settings file in a VCS and import, change and export it with
  Eclipse.</p>

<h3>Debugging</h3>

<p>At one point I thought that the format file should show up in the
argument list of the jdt.ls Java process. But this is not the
case. The settings are shoved into the language server via the
language server protocol. One thing that worked to at least verify
that the file was touched was to watch it
  with <code>inotifywait</code>.</p>

      </div>
    </div>
    <div class="footer">
      <span>&copy;2022,2023,2024 Harald Kirsch</span>
      <span>Blog's <a href="https://codeberg.org/harald/pages" target="_blank">source code</a></span>
    </div>
  </body>
</html>
