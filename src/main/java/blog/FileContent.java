package blog;

import java.io.IOException;
import java.lang.ref.SoftReference;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileContent {
  private final Path blogSourceDir;
  private SoftReference<String> content = new SoftReference<>(null);
  public final Path fileTail;

  public FileContent(Path blogSourceDir, Path fileTail) {
    this.blogSourceDir = blogSourceDir;
    this.fileTail = fileTail;
  }

  String getContent() throws IOException {
    String text = content.get();
    if (text != null) {
      return text;
    }
    text = Files.readString(blogSourceDir.resolve(fileTail), StandardCharsets.UTF_8);
    this.content = new SoftReference<>(text);
    return text;
  }
}
