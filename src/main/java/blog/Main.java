package blog;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Transforms blog entries from source directories to static HTML in a target directory.
 */
public class Main {
  private static final Logger LOG = Logger.getLogger(Main.class.getName());

  private static final String DRAFT_MARKER = "--DRAFT--";
  private static final Pattern PATTERN_TEMPLATE_CONTENT = Pattern.compile("<templatecontent />");
  private static final Pattern PATTERN_TEMPLATE_PAGETITLE = Pattern
    .compile("<title>pagetitle</title>");
  private static final Pattern PATTERN_ABSHREF = Pattern.compile("href=\"/");
  private static final Pattern PATTERN_ABSSRC = Pattern.compile("src=\"/");
  private static final Pattern PATTERN_FIND_TITLE = Pattern
    .compile("<h1[^\"'>]*[\"']title[\"']>([^<]*)");
  private static final String BLOG_URLPATH_PREFIX = "/blog/";

  private static final int MAX_RSS_TEXT_SIZE = 500;
  private final Path fixedPagesSourceDir;
  private final Path browsableSourceDir;
  private final Path targetDirectory;
  private final String pageTemplate;

  Main(Path fixedPagesSourceDir, Path blogSourceDir, Path targetDirectory, String pageTemplate) {
    this.fixedPagesSourceDir = fixedPagesSourceDir;
    this.browsableSourceDir = blogSourceDir;
    this.targetDirectory = targetDirectory;
    this.pageTemplate = pageTemplate;
  }

  public static void main(String[] argv) throws IOException {
    configureLogging();
    if (argv.length != 1) {
      throw new IOException("usage: Main blog_source_dir");
    }
    Path blogSourceDir = Paths.get(argv[0]);
    Path wrapperFile = blogSourceDir.resolve("templates/Page.html");
    String wrapperHtml = Files.readString(wrapperFile, StandardCharsets.UTF_8);
    Main main = new Main(
      blogSourceDir.resolve("fixed-content"),
      blogSourceDir.resolve("browsable-content"),
      Paths.get("./blog"),
      wrapperHtml
    );
    main.transform();
  }

  static String simpleReplaceAll(String text, Pattern p, String replacement) {
    return simpleReplace(text, p, replacement, Integer.MAX_VALUE);
  }

  static String simpleReplace(String text, Pattern p, String replacement, int count) {
    if (count == 0) {
      return text;
    }
    Matcher m = p.matcher(text);
    StringBuilder result = new StringBuilder(2 * text.length());
    int start = 0;
    while (m.find() && count-- > 0) {
      LOG.finest("replacing " + m.group(0) + " with " + replacement);
      result.append(text, start, m.start());
      result.append(replacement);
      start = m.end();
    }
    if (start < text.length()) {
      result.append(text, start, text.length());
    }
    return result.toString();
  }

  /**
   * Prefixes a local absolute href, i.e. one like {@code href="/bla"}, with the path where the blog
   * currently lives: {@link #BLOG_URLPATH_PREFIX}.
   */
  private static String fixLocalAbsoluteHrefs(String html) {
    String hrefFixed = "href=\"" + BLOG_URLPATH_PREFIX;
    html = simpleReplaceAll(html, PATTERN_ABSHREF, hrefFixed);
    String srcFixed = "src=\"" + BLOG_URLPATH_PREFIX;
    return simpleReplaceAll(html, PATTERN_ABSSRC, srcFixed);
  }

  private String generateRss(List<FileContent> fileContents) throws IOException {
    StringBuilder sb = new StringBuilder(20 * 40);
    sb.append("""
      <?xml version="1.0" encoding="utf-8"?>
      <rss version="2.0">
        <channel>
          <title>Haralds Blog</title>
          <link>https://miamao.de/blog</link>
          <description>genug Unfug</description>
          <copyright>Harald Kirsch</copyright>
       """);

    for (int i = 0; i < 20; i++) {
      FileContent fileContent = fileContents.get(i);
      String text = fileContent.getContent();
      if (text.contains(DRAFT_MARKER)) {
        continue;
      }
      if (fileContent.fileTail.getNameCount() == 0) {
        LOG.warning("Ignoring an empty path");
      }
      generateRssItem(sb, fileContent, text);
    }

    sb.append("  </channel>\n").append("</rss>");
    return sb.toString();
  }

  private void generateRssItem(
    StringBuilder sb,
    FileContent fileContent,
    String text
  ) throws IOException {
    int h1Pos = text.indexOf("</h1>");
    if (h1Pos < 0) {
      LOG
        .log(
          Level.WARNING,
          "cannot find </h1> in text, no rss description for " + fileContent.fileTail
        );
      text = "";
    } else {
      text = text.substring(h1Pos + 5).replaceAll("</?[^>]*?>", " ").replaceAll("[&][a-z]+;", " ");
      if (text.length() > MAX_RSS_TEXT_SIZE) {
        text = text.substring(0, MAX_RSS_TEXT_SIZE);
      }
    }
    sb
      .append("    <item>\n")
      .append("      <link>")
      .append(BLOG_URLPATH_PREFIX)
      .append(fileContent.fileTail);
    sb.append("</link>\n");
    sb
      .append("      <title>")
      .append(pageTitle(fileContent.fileTail))
      .append("</title>\n")
      .append("      <pubDate>")
      .append(pubDate(fileContent.fileTail))
      .append("</pubDate>\n")
      .append("      <description>")
      .append(text)
      .append("</description>\n")
      .append("    </item>\n");
  }

  private String pubDate(Path path) throws IOException {
    Path fullPath = browsableSourceDir.resolve(path);
    FileTime lastModifiedTime = Files.getLastModifiedTime(fullPath);
    ZonedDateTime when = lastModifiedTime.toInstant().atZone(ZoneId.of("UTC"));
    return DateTimeFormatter.RFC_1123_DATE_TIME.format(when);
  }

  private static String generateSiteToc(List<FileContent> fileContents) throws IOException {
    StringBuilder sb = new StringBuilder(fileContents.size() * 50);
    sb.append("<!DOCTYPE html>\n");
    // NOTE: level 0 is the innermost level
    String lastToc1 = null;
    for (FileContent fileContent : fileContents) {
      String text = fileContent.getContent();
      if (text.contains(DRAFT_MARKER)) {
        continue;
      }
      int len = fileContent.fileTail.getNameCount();
      if (len == 0) {
        LOG.warning("Ignoring an empty path");
      }
      String toc1;
      if (len < 2) {
        LOG.warning("path with length<2: " + fileContent.fileTail);
        toc1 = "";
      } else {
        toc1 = fileContent.fileTail.getName(len - 2).toString();
      }
      if (!toc1.equals(lastToc1)) {
        if (lastToc1 != null) {
          closeToc1Div(sb);
        }
        generateToc1(sb, toc1);
        lastToc1 = toc1;
      }
      generateToc0(sb, fileContent.fileTail);
    }
    closeToc1Div(sb);
    return sb.toString();
  }

  private static void generateToc0(StringBuilder sb, Path fileTail) {
    String name = fileTail.getName(fileTail.getNameCount() - 1).toString();
    name = name.replaceAll("_", " ").replaceAll("[.]html$", "").replaceFirst("^[0-9]+[.]", "");
    sb
      .append("    <a class='l0' href=\"/")
      .append(fileTail)
      .append("\">")
      .append(pageTitle(fileTail))
      .append("</a>\n");
  }

  private static String pageTitle(Path fileTail) {
    String name = fileTail.getName(fileTail.getNameCount() - 1).toString();
    name = name.replaceAll("_", " ").replaceAll("[.]html$", "").replaceFirst("^[0-9]+[.]", "");
    return name;
  }

  private static void generateToc1(StringBuilder sb, String text) {
    String folded = !sb.isEmpty() ? "folded" : "";
    sb.append("<div class='directory ").append(folded).append("'>\n");
    sb.append("  <div class='l1'>").append(text).append("</div>\n");
    sb.append("  <div class='sitetoc'>\n");
  }

  private static void closeToc1Div(StringBuilder sb) {
    sb.append("  </div>\n"); // of <div class="sitetoc">
    sb.append("</div>\n");
  }

  private static void configureLogging() {
    Logger rootLogger = Logger.getLogger("");
    for (Handler h : rootLogger.getHandlers()) {
      rootLogger.removeHandler(h);
    }
    ConsoleHandler ch = new ConsoleHandler();
    ch.setLevel(Level.ALL);
    rootLogger.addHandler(ch);
    ch.setFormatter(new Formatter() {
      @Override
      public String format(LogRecord lr) {
        return String.format("%s %s%n", lr.getLevel(), lr.getMessage());
      }
    });
  }

  private void transform() throws IOException {
    PageCollector pagesCollector = new PageCollector(browsableSourceDir);
    Files.walkFileTree(browsableSourceDir, pagesCollector);
    Path siteTocTail = Paths.get("sitetoc.html");
    String siteToc = generateSiteToc(pagesCollector.fileContents);
    siteToc = fixLocalAbsoluteHrefs(siteToc);
    writeHtml(siteToc, siteTocTail);
    Path rssTail = Paths.get("rss.xml");
    String rss = generateRss(pagesCollector.fileContents);
    writeHtml(rss, rssTail);
    String previews = generatePreviews(pagesCollector.fileContents, 3);
    contentToPage(previews, Paths.get("index.html"));
    for (FileContent fileContent : pagesCollector.fileContents) {
      contentToPage(fileContent.getContent(), fileContent.fileTail);
    }

    PageCollector fixedPages = new PageCollector(fixedPagesSourceDir);
    Files.walkFileTree(fixedPagesSourceDir, fixedPages);
    for (FileContent fileContent : fixedPages.fileContents) {
      contentToPage(fileContent.getContent(), fileContent.fileTail);
    }
  }

  private String generatePreviews(List<FileContent> fileContents, int count) throws IOException {
    Matcher wrapTitle = Pattern.compile("<h1\\s+class=\"title\">[^<]*</h1>").matcher("");
    StringBuilder sb = new StringBuilder(20000);
    for (FileContent fileContent : fileContents) {
      String text = fileContent.getContent();
      if (text.contains(DRAFT_MARKER)) {
        continue;
      }
      addPreview(sb, fileContent, wrapTitle);
      if (--count <= 0) {
        break;
      }
    }
    return sb.toString();
  }

  private void contentToPage(String content, Path fileTail) throws IOException {
    LOG.info("transforming " + fileTail);
    String fullHtml = simpleReplace(pageTemplate, PATTERN_TEMPLATE_CONTENT, content, 1);
    String pageTitle = findPageTitle(content, fileTail);
    fullHtml = simpleReplace(fullHtml, PATTERN_TEMPLATE_PAGETITLE, pageTitle, 1);
    fullHtml = fixLocalAbsoluteHrefs(fullHtml);
    writeHtml(fullHtml, fileTail);
  }

  private void writeHtml(String html, Path fileTail) throws IOException {
    Path target = targetDirectory.resolve(fileTail);
    Files.createDirectories(target.getParent());
    Files
      .writeString(
        target,
        html,
        StandardCharsets.UTF_8,
        StandardOpenOption.CREATE,
        StandardOpenOption.TRUNCATE_EXISTING
      );
  }

  private void addPreview(
    StringBuilder sb,
    FileContent fileContent,
    Matcher titleWrapper
  ) throws IOException {
    sb.append("<div class='preview'>\n");
    String content = fileContent.getContent();
    titleWrapper.reset(content);
    String titleHref = "<a href=\"/" + fileContent.fileTail + "\">\n  $0\n</a>";
    content = titleWrapper.replaceFirst(titleHref);
    if (titleWrapper.start() == 0) {
      LOG.warning("no title in " + fileContent);
    }
    sb.append(content);
    sb.append("</div>\n");
  }

  private String findPageTitle(String content, Path pageLogRef) {
    Matcher m = PATTERN_FIND_TITLE.matcher(content);
    StringBuilder sb = new StringBuilder(200);
    sb.append("<title>Haralds Blog");
    if (!pageLogRef.endsWith("index.html")) {
      if (m.find()) {
        sb.append(" &mdash; ").append(m.group(1));
      } else {
        LOG.warning("no title found in " + pageLogRef);
      }
    }
    sb.append("</title>");
    return sb.toString();
  }
}
