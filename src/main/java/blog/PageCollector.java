package blog;

import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PageCollector extends SimpleFileVisitor<Path> {
  public final List<FileContent> fileContents = new ArrayList<>();

  private final Path blogSourceDir;
  private final int sourcePathLength;

  PageCollector(Path blogSourceDir) {
    this.blogSourceDir = blogSourceDir;
    this.sourcePathLength = blogSourceDir.getNameCount();
  }

  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
    Path fileTail = file.subpath(sourcePathLength, file.getNameCount());
    FileContent fileContent = new FileContent(blogSourceDir, fileTail);
    int pos = Collections
      .binarySearch(fileContents, fileContent, (p1, p2) -> -p1.fileTail.compareTo(p2.fileTail));
    if (pos < 0) {
      pos = -(pos + 1);
    }
    fileContents.add(pos, fileContent);
    return FileVisitResult.CONTINUE;
  }
}
