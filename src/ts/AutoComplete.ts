import { addStyleElementToHead, Func, Runnable } from './Util.js';

export interface AutoCompleter {
  (term: string): string[];
}

export class AutoComplete {
  static cssSelected = 'acSelected';
  private static style? = `
  .AutoComplete {
    display: flex;
    flex-direction: column;
    margin: 0;
    padding: 0;
    position: absolute;
    background: inherit;
    border: 1px solid #aaa;
    box-shadow: -4px 4px 3px grey;
    cursor: pointer;
    overflow-y: scroll;
    max-height: 150px;
  }
  .AutoComplete div {
    padding-left: 4px;
    line-height: 28px;
  }
  .AutoComplete div:nth-child(odd) {
    background-color: color-mix(in srgb, #000 4%, var(--background));
  }
  .AutoComplete div.${AutoComplete.cssSelected} {
    background: var(--selected-element, #aba);
  }
  `;
  private readonly element: HTMLDivElement;
  // Having these all prepared and ready instead of just re-creating the HTML
  // elements is necessary thanks to a weird bug of Firefox on Android. See
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1814710
  // https://jsfiddle.net/1ekc93sx/2/
  private readonly acItems: AcItem[] = [];

  /**
   * Creates an instance.
   * @param target an input element for which to provide the auto completion
   * @param submit the command to immediately submit the selected item after
   * entering it into the target, i.e. the command called shall pick the
   * value normally from the target
   * @param provider to compute a list of suggestions given a few characters
   * @param onEnter to be called when the user selects a value
   */
  constructor(
    private readonly target: HTMLInputElement,
    private readonly provider: AutoCompleter,
    private readonly onEnter: Runnable
  ) {
    addStyleElementToHead(AutoComplete.style);
    delete AutoComplete.style;

    this.attach(target);
    const acOnclick = (evt: Event, term: string): void => this.click(evt, term);
    const element = document.createElement('div');
    element.classList.add(AutoComplete.name);
    element.ontouchmove = (evt) => {
      evt.stopPropagation();
    };

    target.insertAdjacentElement('afterend', element);
    for (let i = 0; i < 60; i++) {
      const acItem = new AcItem(element, acOnclick);
      this.acItems.push(acItem);
      element.append(acItem.getElement());
    }
    this.element = element;
    this.hide();
  }

  public hide(): void {
    this.element.classList.add('hidden');
  }

  private show(): void {
    this.element.classList.remove('hidden');
  }

  private attach(target: HTMLInputElement): void {
    target.oninput = (_evt) => this.render();
    target.addEventListener('keydown', (evt) => this.keypress(evt));
  }

  private keypress(evt: KeyboardEvent): void {
    switch (evt.key) {
      case 'Enter': {
        this.keypressEnter(evt);
        return;
      }
      case 'ArrowDown': {
        this.keypressArrowDown(evt);
        return;
      }
      case 'ArrowUp': {
        this.keypressArrowUp(evt);
        return;
      }
      default:
        return;
    }
  }

  private keypressArrowUp(evt: Event): void {
    this.doArrowUpDown(
      evt,
      (s: Element) => s.previousElementSibling,
      undefined
    );
  }

  private keypressArrowDown(evt: Event): void {
    const classList = this.element.firstElementChild?.classList;
    this.doArrowUpDown(evt, (s: Element) => s.nextElementSibling, classList);
  }

  private doArrowUpDown(
    evt: Event,
    fetchOther: Func<Element, Element | null>,
    classList: DOMTokenList | undefined
  ): void {
    evt.preventDefault();
    const selected = this.element.querySelector('.' + AutoComplete.cssSelected);
    if (selected === null) {
      classList?.add(AutoComplete.cssSelected);
      return;
    }
    const other = fetchOther(selected);
    if (other === null || other.classList.contains('hidden')) {
      return;
    }

    selected.classList.remove(AutoComplete.cssSelected);
    other.classList.add(AutoComplete.cssSelected);
    other.scrollIntoView({ block: 'center' });
  }

  private keypressEnter(evt: Event): void {
    const selected = this.element.querySelector('.' + AutoComplete.cssSelected);
    if (selected === null) {
      return;
    }
    this.hide();
    if (selected instanceof HTMLElement) {
      this.valueWasSelected(selected.innerText);
    }
    selected.classList.remove(AutoComplete.cssSelected);
    evt.preventDefault();
  }

  private click(evt: Event, term: string): void {
    this.valueWasSelected(term);
    this.hide();

    // yeah, yeah, belt + suspenders
    evt.preventDefault();
    evt.stopPropagation();
    evt.stopImmediatePropagation();
  }

  private valueWasSelected(term: string): void {
    this.target.value = term;
    this.onEnter();
  }

  private render(): void {
    const terms = this.provider(this.target.value);
    if (terms.length === 0) {
      this.hide();
      return;
    }
    this.acItems.forEach((item) => item.hide());
    let i = 0;
    for (const term of terms) {
      const acItem = this.acItems[i++];
      if (acItem === undefined) {
        break;
      }
      acItem.setTerm(term);
    }

    const horizMargin = 6;
    const xpos = horizMargin + this.target.offsetLeft;
    const ypos = this.target.offsetTop + this.target.offsetHeight;
    this.element.style.left = xpos + 'px';
    this.element.style.top = ypos + 'px';
    this.element.style.width = this.target.clientWidth - 2 * horizMargin + 'px';
    this.show();
  }
}

class AcItem {
  private element: HTMLDivElement;

  constructor(
    parent: HTMLElement,
    onClick: (evt: Event, term: string) => void
  ) {
    this.element = document.createElement('div');
    parent.append(this.element);
    this.element.onclick = (evt) => onClick(evt, this.getTerm());
  }

  getTerm(): string {
    return this.element.innerText;
  }

  setTerm(term: string): void {
    this.element.innerText = term;
    this.show();
  }

  getElement(): HTMLDivElement {
    return this.element;
  }

  hide(): void {
    this.element.classList.add('hidden');
    this.element.classList.remove(AutoComplete.cssSelected);
  }

  show(): void {
    this.element.classList.remove('hidden');
  }
}
