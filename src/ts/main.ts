import { Toot } from './Toot.js';
import { createTranslator } from './Translations.js';

// FIXME: start using esbuild and import highlight.js properly as a module
declare const hljs: {
  highlightElement(el: Element): void;
};

// The translater has a nice short name
const τ = createTranslator('de');

// FIXME: store toggle state of directories locally
// and fix it after walking to another page
async function startup(): Promise<void> {
  const dirToggles = document.querySelectorAll('.directory .l1');
  for (const el of dirToggles) {
    if (!(el instanceof HTMLElement)) {
      continue;
    }
    const toToggle = el.parentElement;
    if (toToggle === undefined || toToggle === null) {
      continue;
    }
    el.onclick = () => toToggle.classList.toggle('folded');
  }
  await injectLiveTemplates();
  withAll('.preview', (el) => (el.onclick = () => fullPreviewClick(el)));
  withEl('.sitetoc', (sitetoc) => {
    withAll(
      '.sitetoc h1, .article .sitetoc-button',
      (el) => (el.onclick = () => toggleSiteToc(sitetoc))
    );
  });

  // if we're on a full article, not the start page
  if (null !== document.querySelector('.preview')) {
    withEl('.sitetoc-button', (el) => el.click());
  }

  withAll('mjx-container', (el) => {
    const sw = el.scrollWidth;
    // The +2 is weirdo rendering of Firefox.
    const w = 2 + el.getBoundingClientRect().width;
    el.classList.toggle('scrollable', sw > w);
  });

  // FIXME: use highlight.js as a module
  document.querySelectorAll('pre[class^=lang-]').forEach((el) => {
    hljs.highlightElement(el);
  });

  addMastodonTootButton();
}

function toggleSiteToc(sitetoc: HTMLElement): void {
  sitetoc.classList.toggle('hidden');
}

function withEl(selector: string, f: (el: HTMLElement) => void): void {
  const el = document.querySelector(selector);
  if (el instanceof HTMLElement) {
    f(el);
  }
}
function withAll(selector: string, f: (el: HTMLElement) => void): void {
  elWithAll(document, selector, f);
}

function elWithAll(
  root: ParentNode,
  selector: string,
  f: (el: HTMLElement) => void
): void {
  root.querySelectorAll(selector).forEach((el) => {
    if (el instanceof HTMLElement) {
      f(el);
    }
  });
}

function injectLiveTemplates(): Promise<void> {
  const templates = document.querySelectorAll('[class="livetemplate"]');
  let head = Promise.resolve();
  for (const el of templates) {
    const href = el.getAttribute('href');
    if (href === null) {
      continue;
    }
    head = head
      .then(() => injectLiveTemplate(el, href))
      // FIXME: append is wrong, it creates an element with the given innerText
      .catch((_error) => el.append('<span>failed to load + href</span>'));
  }
  return head;
}

function injectLiveTemplate(
  liveTemplate: Element,
  href: string
): Promise<void> {
  return fetch(href)
    .then((response) => {
      if (!response.ok) {
        throw new Error('did not get code 200');
      }
      return response.text();
    })
    .then((text) => {
      const part = document.createElement('template');
      part.innerHTML = text.trim();
      liveTemplate.replaceWith(part.content);
    });
}

function fullPreviewClick(el: HTMLElement): void {
  const ahref = el.querySelector('a[href]');
  if (ahref instanceof HTMLElement) {
    ahref.click();
  }
}

// FIXME: these could go into Toot too, or at least the doToot and doneToot
function addMastodonTootButton(): void {
  const toot = new Toot(τ, (value) => doneToot(value));
  document.body.append(toot.getElement());
  withEl('button.tootbutton', (el) => {
    el.append(τ('toot'));
    el.onclick = (evt) => doToot(toot, evt);
  });
}

function doToot(toot: Toot, evt: MouseEvent): void {
  evt.preventDefault();
  toot.show();
}

function doneToot(domain: string | undefined): void {
  if (domain === undefined) {
    return;
  }
  if (domain === '' || domain === null){
    return;
  }
  const url = new URL('https://' + domain + '/publish/')
  url.searchParams.append(
    'text',
    '@haraldki@nrw.socal\n...' + τ('toot_filler') + '...\n\n' + location.href
  );
  window.open(url, '_blank');
}

document.addEventListener('DOMContentLoaded', () => startup(), {
  once: true,
});
