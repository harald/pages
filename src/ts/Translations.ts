
const translationTable = {
  toot: {
    en: 'toot',
    de: 'Tröt',
  },
  mastodonDomainPrompt : {
    en: 'Toot on which server',
    de: 'Auf welchem Server tröten?'
  },
  toot_filler: {
    en: 'tell me what you think',
    de: 'schreib mir was',
    fr: 'écris moi'
  },
  Cancel: {
    en: 'Cancel',
    de: 'Abbrechen'
  },
  OK: {
    en: 'OK',
    de: 'OK'
  },
  'e.g.': {
    en: 'e.g.',
    de: 'z.B.'
  }
}

type KeysOfUnion<T> = T extends T ? keyof T: never;
type TRANSLATION_TAG = keyof typeof translationTable;
type LANGUAGE_TAG = KeysOfUnion<typeof translationTable[TRANSLATION_TAG]>
type DEFAULT_LANGUAGE_TAG = keyof typeof translationTable[TRANSLATION_TAG];

const allLanguages = new Set<string>(Object.keys(translationTable.toot));

for(const key in translationTable) {
  const others = Object.keys(translationTable[key]);
  for (const lang of allLanguages) {
    if (!others.includes(lang)) {
      allLanguages.add(lang);
    }
  }
}

function isLanguage(text: string): text is LANGUAGE_TAG {
  return allLanguages.has(text);
}

function translate(language: LANGUAGE_TAG, fallbackLanguage: LANGUAGE_TAG, tag: TRANSLATION_TAG): string {
  const entry = translationTable[tag];
  return entry[language] || entry[fallbackLanguage] || tag;
}

export type Translator = (t: TRANSLATION_TAG) => string;
export function createTranslator(
  defaultLanguage: DEFAULT_LANGUAGE_TAG
): Translator {
  let userLang = navigator.language;
  if (userLang.length > 2) {
    userLang = userLang.substring(0, 2);
  }
  if (isLanguage(userLang)) {
    const language = userLang;
    return (tag) => translate(language, defaultLanguage, tag);
  }
  return (tag) => translate(defaultLanguage, defaultLanguage, tag);
}