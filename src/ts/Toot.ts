import { AutoComplete } from './AutoComplete.js';
import { Translator } from './Translations.js';
import { addStyleElementToHead, Consumer } from './Util.js';

class MatchedTerm {
  constructor(readonly pos: number, readonly instance: string) {}
  cmp(other: MatchedTerm): -1 | 0 | 1 {
    if (this.pos < other.pos) {
      return -1;
    } else if (this.pos > other.pos) {
      return 1;
    }
    if (this.instance.length < other.instance.length) {
      return 1;
    } else if (this.instance.length > other.instance.length) {
      return -1;
    }
    const c = this.instance.localeCompare(other.instance);
    return c < 0 ? 1 : c > 0 ? 1 : 0;
  }
}

export class Toot {
  private static style? = `
    .TootDialog {
      position: absolute;
      top: 0px;
      width: 100%;
      height: 100%;
      background-color: rgba(210,210,210,0.8);
    }
    .TootDialog .toot {
      position: relative;
      top: 60px;
      width: 30ch;
      margin: 0 auto;
      opacity: 100%;
      background: var(--background, #eee);
      border-width: 0;
      border-radius: 8px;
      padding: 8px;
      display: flex;
      flex-direction: column;
      gap: 8px;
    }
    .TootDialog input {
      font-size: inherit;
      padding: 4px;
      border-radius: 4px;
      border-style: solid;
    }
    .TootDialog input:focus-visible {
      outline: 0px;
      border-color: var(--button-border-color);
    }
    .TootDialog .buttons {
      display: flex;
      justify-content: space-between;
      
    }
    .TootDialog .buttons button {
      min-width: 8ch;
    }
    .TootDialog .buttons button:focus {
      background-color: color-mix(in srgb, #000 4%, var(--background));
    }
    .TootDialog .buttons button:first-child {
      border-color: var(--deco-foreground-light);
      border-width: 1px;
    }
  `;

  private static someInstances = [
    'mastodon.social',
    'mstdn.social',
    'mas.to',
    'loforo.com',
    'mastodon.world',
    'hachyderm.io',
    'troet.cafe',
    'social.tchncs.de',
    'norden.social',
    'nrw.social',
    'det.social',
    'sueden.social',
    'mastodontech.de'
  ];
  private readonly backdrop: HTMLDivElement;
  private readonly inputEl: HTMLInputElement;
  private readonly okButton: HTMLButtonElement;
  /**
   * Creates a dialog element to let the user enter their preferred
   * mastodon instance to toot about the site.
   */
  constructor(
    τ: Translator,
    private readonly onok: Consumer<string | undefined>
  ) {
    addStyleElementToHead(Toot.style);
    delete Toot.style;

    this.backdrop = document.createElement('div');
    this.backdrop.classList.add('TootDialog');

    const element = document.createElement('div');
    element.classList.add('toot');
    const questionEl = document.createElement('div');
    questionEl.innerText = τ('mastodonDomainPrompt');
    this.inputEl = document.createElement('input');
    this.inputEl.type = 'text';
    this.inputEl.minLength = 20;
    this.inputEl.placeholder = τ('e.g.') + 'mastodon.social';
    this.inputEl.onkeydown = (evt) => this.keypress(evt);

    const buttonRow = document.createElement('div');
    buttonRow.classList.add('buttons');
    const cancelButton = document.createElement('button');
    cancelButton.innerText = τ('Cancel');
    cancelButton.onclick = () => this.hide();

    const okButton = document.createElement('button');
    okButton.innerText = τ('OK');
    okButton.onclick = () => this.ok();
    this.okButton = okButton;

    buttonRow.append(cancelButton, okButton);
    element.append(questionEl, this.inputEl, buttonRow);
    this.backdrop.append(element);
    this.backdrop.ontouchmove = (evt) => evt.preventDefault()

    this.backdrop.onclick = (evt) => {
      if (evt.target === this.backdrop) {
        evt.preventDefault();
        this.hide();
      }
    };
    this.hide();
    this.attachAutocomplete();
  }

  getElement(): HTMLDivElement {
    return this.backdrop;
  }

  hide(): void {
    this.backdrop.classList.add('hidden');
    this.onok(undefined);
  }

  show(): void {
    this.backdrop.classList.remove('hidden');
    this.inputEl.focus();
  }

  private keypress(evt: KeyboardEvent): void {
    if (evt.key === 'Escape') {
      if (this.inputEl.value.length > 0) {
        this.inputEl.value = '';
        this.inputEl.dispatchEvent(new Event('input'));
      } else {
        this.hide();
      }
    }
  }

  private attachAutocomplete(): void {
    new AutoComplete(
      this.inputEl,
      (t) => this.matchTerm(t),
      () => this.autocompleteValueEntered()
    );
  }

  private autocompleteValueEntered(): void {
    this.okButton.focus();
  }

  private matchTerm(term: string): string[] {
    term = term.toLowerCase().trim();
    if (term.length < 1) {
      return [];
    }
    const found: MatchedTerm[] = [];
    for (const instance of Toot.someInstances) {
      const pos = instance.indexOf(term);
      if (pos >= 0) {
        const el = new MatchedTerm(pos, instance);
        found.push(el);
      }
    }
    found.sort((a, b) => a.cmp(b));
    return found.map(mt => mt.instance);
  }

  private ok(): void {
    this.backdrop.classList.add('hidden');
    this.onok(this.inputEl.value);
  }
}