export interface Func<FROM, TO> {
  (x: FROM): TO;
}
export type Runnable = () => void;
export type Consumer<T> = (value: T) => void;

export type StyleProvider = {
  style?: string;
}

export function addStyleElementToHead(style: string | undefined): void {
  if (style === undefined) {
    return;
  }
  const styleEl = document.createElement('style');
  styleEl.textContent = style;
  document.head.prepend(styleEl);
}
