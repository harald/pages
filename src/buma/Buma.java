import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import bm.Build;
import bm.Build.Cmdline;
import bm.machine.BuildProblemException;
import bm.target.FileContentState;
import bm.target.FileTarget;
import bm.target.FixedStateTarget;
import bm.util.Exec;
import bm.util.FindFiles;
import bm.util.StringLister;
import bm.util.Util;

/**
 * Project builder using <a href="https://codeberg.org/harald/BuildMachine">BuildMachine.</a> bla
 * bla blabla
 */
public class Buma {
  private static final Exec exec =
      Exec.create(List.of(Paths.get("node_modules/.bin")));
  private static final Path blogSourceDir = Paths.get("blogSource");
  private static final Path HIGHLIGH_JS_ZIP =
      Path.of(System.getProperty("user.home"), "transfer", "highlight_js.zip");
  private static final Path highlightJsMain = Path.of("highlight.min.js");
  private static final Path highlightJsCss = Path.of("styles", "github.min.css");

  public static void main(String[] argv) throws BuildProblemException, IOException {
    Build.initLogging(System.err);
    Build build = new Build("transform", Paths.get("build/hashes"));
    build.addBuildInspectionTargets();
    Cmdline cmdline = build.parseCmdline(argv);

    build.add(FixedStateTarget.runAlways("clean",
        () -> Util.recursiveDelete("build")
    ));

    var javaSources = FileContentState.fromContent("src/main/java").toFileTarget();
    var javaClasses = FileContentState
        .fromContent("build/classes")
        .toFileTarget(List.of(javaSources), t -> compileJava(t, javaSources));

    var highlightJsZip =
        FileContentState.fromContent(HIGHLIGH_JS_ZIP).toFileTarget();
    FileTarget highlightJsFiles = FileContentState
        .fromContent(Path.of(".", "blog", "js-gen"))
        .withPathFilter(p -> Files.isDirectory(p) || p
            .getFileName()
            .equals(highlightJsMain) || p
                                 .getFileName()
                                 .equals(highlightJsCss.getFileName()))
        .toFileTarget(List.of(highlightJsZip),
            t -> extractHighlightJs(t, highlightJsZip)
        )
        .withName("highlight");
    build.add(highlightJsFiles);

    var tsCompiled =
        FileContentState.fromContent("blog/js-gen").toFileTarget(List.of(
            FileContentState.fromContent("src/ts").toFileTarget(),
            FileContentState.fromLastModified("tsconfig.json").toFileTarget()
        ), _x -> exec.exec(List.of("tsc")));
    build.addAlias("tsc", tsCompiled);

    FileTarget blogOut = FileContentState.fromContent("./blog").toFileTarget(List.of(
        Util.createFileCopyTarget("./blog/style.css",
            blogSourceDir.resolve("style.css")
        ),
        Util.createFileCopyTarget("./blog/images", blogSourceDir.resolve("images")),
        FileContentState
            .fromContent(blogSourceDir.resolve("browsable-content"))
            .withPathFilter(p -> {
              String fname = p.getFileName().toString();
              return !(fname.endsWith("~") || fname.startsWith("#"));
            })
            .toFileTarget(),
        FileContentState.fromContent("blogSource/fixed-content").toFileTarget(),
        FileContentState.fromContent("blogSource/templates").toFileTarget(),
        highlightJsFiles,
        tsCompiled,
        javaClasses
    ), _x -> runJava(javaClasses));
    build.addAlias("transform", blogOut);
    build.addAlias("compileJava", javaClasses);

    build.add(FixedStateTarget.onDependencyChange("publish",
        List.of(blogOut),
        Buma::upload
    ));
    build.updateAll(cmdline.requestedTargets());
  }

  private static void extractHighlightJs(FileTarget target, FileTarget zip)
      throws BuildProblemException {
    StringLister cmdline = new StringLister("unzip",
        "-jqo",
        zip.getPath().toString(),
        "-d",
        target.getPath().toString()
    )
        .add(highlightJsMain.toString())
        .add(highlightJsCss.toString());
    exec.exec(cmdline.get());
  }

  private static void upload() throws BuildProblemException {
    String blogRemote = System.getenv("BLOGREMOTE");
    if (null == blogRemote) {
      throw new BuildProblemException(
          "must have environment BLOGREMOTE as an rsync target folder");
    }

    List<String> rsync = new StringLister(
        "rsync",
        "-av",
        "--delete",
        "--delete-excluded",
        "--exclude=*.d.ts",
        "--exclude=*.js.map",
        "--exclude=.compiled"
    ).add(Path::toString, FindFiles.glob("build/blog/*")).add(blogRemote).get();

    exec.exec(rsync);
  }

  private static void runJava(FileTarget classes) throws BuildProblemException {
    List<String> command = List.of(
        "java",
        "-cp",
        classes.getPath().toString(),
        "blog.Main",
        blogSourceDir.toString()
    );
    exec.exec(command);
  }

  private static void compileJava(FileTarget classes, FileTarget sources)
      throws BuildProblemException {
    List<String> cmdline = new StringLister(
        "javac",
        "-d",
        classes.getPath().toString(),
        "-deprecation",
        "-Xmaxerrs",
        "10"
    ).add(Path::toString, sources.getFiles()).get();
    exec.exec(cmdline);
  }
}
