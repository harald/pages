<div class="date">2024-08-18</div>
<h1 class="title">A Practical Use for Regular Language Intersection</h1>

Long story short: <a href="https://codeberg.org/harald/monqjfa"
                      target="_blank">Monqjfa</a> now has
                      an <a href="#intersection_operator">intersection
                      operator</a>.

<h2>What is "Regular Language Intersection"?</h2>
<p>A <a href="https://en.wikipedia.org/wiki/Regular_language"
        target="_blank">regular language</a>, simply put, is the set
        of strings you can match with a given regular expression. The
        regular expressions you find in programming languages or
        command line programs, though, are often somewhat enhanced with
  tricks to extend the theoretical setup somewhat.</p>

<p>There is a correspondence between piecing together two regular
expressions and combining the respective sets of strings. If we call
$S(r)$ the set of strings matched by a regular expression $r$, then
the regular expression <code>x|y</code> corresponds to the set union
  $S(x)\cup S(y)$. Similarly, <code>xy</code> corresponds to the strings
  $\{XY \mid  X\in S(x) \wedge Y\in S(y)\}$.
</p>

<p>Something normally not provided, is the set intersection. There is
  no regular expression syntax I know of (Java, Tcl, Python, grep,
  awk, sed, Javascript) which provides the respective operator,
  say <code>&amp;</code>, such that <code>x&amp;y</code> corresponds to
  the set of strings $S(x)\cap S(y)$.</p>

<p>But, hey, this surely is pure chichi
  from <a href="https://en.wikipedia.org/wiki/Theoretical_computer_science"
          target="_blank">Theoretical Computer Science</a>. <b>Who needs
  this?</b></p>

<p>Well, recently I stumbled over a use case. Not that everyone out
  there is in dire need. This is from a pet project of mine, but it
  was fun.</p>

<h2>Some Background about NFA/DFA</h2>
<p>For use in string matching, a regular expression is transformed
  into
  a <a href="https://en.wikipedia.org/wiki/Nondeterministic_finite_automaton"
  target="_blank">finite automaton</a>. They come in two flavors,
  non-deterministic (NFA) and (DFA) deterministic. Most libraries or
  programs in use are based on the non-deterministic ones. They are
  more flexible to allow additional, practically useful tricks and are
  fast enough after many years of optimization. On the other hand,
  matching with a DFA is theoretically faster, because where in an NFA
  the algorithm may need to try several possible parallel branches,
  (that's the "non"-determinism), traversing a DFA's graph is
  deterministic and either matches or not. There is no "try out this
  path ... oh, no, try the other one", etc.</p>

<p>My younger self was curious what it takes to
  write a regular expression matcher using a DFA. Out came "monqjfa",
  which you can still find on github. What was it missing: the
  intersection operation.</p>

<h2>Modernized monqjfa and the ooo program</h2>
<p>Yet recently I decided to modernize the thing and set it up on
  <a href="https://codeberg.org/harald/monqjfa"
     target="_blank">codeberg</a>. As a proof of concept, I wrote
     a <code>grep</code> like utility
     called <a href="https://codeberg.org/harald/monqjfa/src/branch/main/src/main/java/monq/Oooo.java#:~:text=class,Oooo" target="_blank">Oooo.java</a> for lack of a better name. The
     script to call it got the even more ingenious
     name <code>ooo</code>. Rather than partitioning the input by
     lines, as <code>grep</code> does, you may provide
     a <code>split=regex</code> on the command line to define what
     constitutes a "line". And you can provide several matching and
     transformation rules, for
     example <code>"r=[0-9]+->&lt;num>{}&lt;/num></code> where
     the <code>{}</code> is replaced by the match.</p>

<p>The algorithm could first search for a <code>split</code> match and
then, in a second, third and more steps, search for the other
regular expressions in the string terminated by
  the <code>split</code> match. But no, this is not how you want to do
  things if you have a DFA. Instead, all regular
  expressions, <code>split</code> included, are combined into one big
  DFA, with their expansion templates stored in the stop states. Then
  just one matching step is used each time, the longest match is
  found, the stored template is interpreted and the next match can be
  started on the rest of the input. No two or more passes over the
  lines are necessary.</p>

<p>Well, well. Now we want to
  find <code>import.*foo</code>. This looks completely harmless and
  everyone has done this with <code>grep</code>.
  What is the problem with a single DFA which basically encodes a
  regular expression like <code>\n|import.*foo</code>,
  where <code>\n</code> is the <code>split</code> regex to match the
  end of a line? What happens is, that if one line
  contains <code>import</code> but not <code>foo</code>, the matching
  treats the next <code>\n</code> as just another character of
  the <code>.*</code> in <code>import.*foo</code> and keeps gobbling
  characters until it eventually finds the <code>foo</code>.</p>

<p>Some regular expressions engines, for a reason like this, treat the
  dot-character not as "match every character", but rather as "match
  every character except line end". But
  since <code>ooo</code>'s <code>split</code> allows just any odd
  regular expression, this does not work here.</p>

<h2 id="intersection_operator">The Intersection Operator</h2>

<p>There we are at the use case for the intersection operator. And for
  good measure, the set complement operator too. How's that?</p>

<p>What we really would like to encode with <code>a.*b</code> in the
  above example is:</p>

<blockquote>Match any string starting with <code>a</code> and
  ending with <code>b</code> except if it contains a match of
  the <code>split</code> regular expression.</blockquote>

<p>Translated into set operations the "match ... except ... contains"
  can be translated into a set theoretic description as follows:
</p>
<ul>
  <li>$S(\text{.*\\n.*})$ is the set of strings containing at least one
    newline.</li>
  <li>But we don't want to match any of those, rather the complement:
    $S(\text{.*\\n.*})^c$.</li>
  <li>But not just the complement, rather any string in this
    complement <em>and</em> matching our primary regular expression:
    $S(\text{a.*b})\cap S(\text{.*\\n.*})^c$.</li>
</ul>

<p>And here is how you would write this in the new monqjfa on
  codeberg:</p>
<pre class="lang-java">
NfaBuilder.matchRegex("a.*b&amp;(\n)^")
</pre>
<p>Monqjfa understands the intersection operator <code>&amp;</code>
  and, given any regular expression <code>r</code>, the caret
  operator <code>r^</code> is a shortcut for <code>(.*r.*)~</code>
  where <code>~</code> provides the complement, such
  that <code>r^</code> means "any string not containing a match
  of <code>r</code>.</p>

<p>With the program <code>ooo</code> every rule is combined with the
  "does not contain a match of the split regex" automatically, so
  there you don't have to deal with the details. But now you know how
  it works internally.</p>

<p>And you may have fun experimenting with NFA, DFA operations. The
  Java class <code>monq.jfa.FaToDot</code> writes out any NFA or DFA
  in <a href="https://graphviz.org" target="_blank">graphviz</a>
  format so that the <code>dot</code> program can be used to view the
  graph of an automaton.</p>

<p>As an example, the graph below was generated with</p>
<pre class="lang-bash">
java -cp monq-3.3.0.jar monq.jfa.FaToDot -dfa 'a.*b&amp;X^' \
   | dot -Tsvg -Grankdir=LR >dfa.svg
</pre>

<p>This is what we talked above, except that the <code>\n</code> was
  replaced by an <code>X</code> for better readability in the
  graph.</p>
<div  style="overflow-y: scroll; padding-bottom: 10px">
  <img src="/images/dfa-intersection-complement.svg"
       style="width: 100%; min-width: 670px;" />
</div>
    
<p>The rectangular state on the left is the start state. With nothing
  but <code>ab</code> you immediately end up in the red stop state. In
  between is the elliptical state with self loops on many characters:
  <code>\u000..W</code>, <code>Y..a</code>
  and <code>c..\uffff</code>. This is for the <code>.*</code> but note
  what is missing: the <code>X</code>. There is nothing for it there,
  so it cannot appear between <code>a</code> and <code>b</code>.</p>

  
