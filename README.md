You are looking at the source code of my blog which is 
<a href="https://harald.codeberg.page/blog/" target="_blank">here</a>.

## Notes To Self

* Add `--DRAFT--` to a new page to prevent it from being rendered to
  the preview or the toc.

* No need for boiler plate `<p>...</p>`, the typical double linefeed
  suffices.

* Blog internal links should be written as `href="/blabla"`, i.e. with
  an absolute link as if the blog's root is right at the host:port
  part. These will be rewritten to work even with an initial path
  after the root.

To generate the blog:
* Run `buma -f buma/Buma.java`
* Check changes in `./blog` with git, commit and push

Reminder how it works, see [pages documentation](https://codeberg.page/):
* This is my `pages` repository on codeberg
* My domain points to harald.codeberge.page per TXT entry as described
  in the above documentation.
* The `./index.html` does a `http-equiv` redirect to `./blog`.

In particular the main branch is published here, not some branch
called `pages`.
